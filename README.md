# [phpoffice](https://phppackages.org/s/phpoffice)/[phpspreadsheet](https://phppackages.org/p/phpoffice/phpspreadsheet)

A pure PHP library for reading and writing spreadsheet files https://phpspreadsheet.readthedocs.io

(Unofficial demo and howto)

* [*Symfony 5 PhpSpreadsheet (Creating Excel File)*](https://medium.com/@biberogluyusuf/symfony-5-phpspreadsheet-creating-excel-file-f4ea14045ce5)
  2020-08 Yusuf Biberoğlu
* [*How to create an Excel file with PHP in Symfony 4*](https://ourcodeworld.com/articles/read/798/how-to-create-an-excel-file-with-php-in-symfony-4)